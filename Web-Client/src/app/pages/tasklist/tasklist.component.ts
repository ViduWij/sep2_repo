import {Component} from '@angular/core'
import {TaskListService} from './tasklist.service'
import {Observable} from 'rxjs/Rx'

@Component({
    selector:'tasklist',
    template:require('./tasklist.html'),
    styles:[require('./tasklist.scss')],
    providers:[TaskListService]
})

export class TaskList{
    public taskArray;
    taskName:any;
    private _max1:number = 5;
    pending=false;

    constructor(private _TaskListService : TaskListService){
        this.taskArray = _TaskListService.GetTask();
    }

updateRate(taskid:string,count:number){
    // this.updatedTaskId=event;
    // console.log("taskid "+taskid+"/"+count);
    this._TaskListService.UpdateRate(taskid,count);
}

ngAfterViewInit(){
    var keyups = Observable.fromEvent($("#search"), "keyup")
    .map(e => e.target.value)
    .filter(text => text.length >= 3)
    .debounceTime(400)
    .distinctUntilChanged()
    .flatMap(searchTerm => {
        this.pending =true;
        this.taskArray = [];
        var promise = this._TaskListService.searchTasks(searchTerm);
        return Observable.fromPromise(promise);
    });
    keyups.subscribe(data => {console.log(data);
       this.taskArray = [];
       data.forEach(element => {
           this.taskArray.push(element);
       });
       this.pending =false;
       console.log(this.taskArray);
    });

}

}