import {Component} from '@angular/core'
import {TasksService} from './tasks.service'
import {Tile} from './emp.component'
import {Http,HTTP_PROVIDERS} from '@angular/http'
import {Observable} from 'rxjs/Rx'

@Component({
    selector:'tasks',
    styles:[require('./tasks.scss')],
    template:require('./tasks.html'),
    providers:[TasksService],
    directives:[Tile]
})
export class Tasks{
    categoryList;
    selectedItem = "Select Category";
    clickedAddNew=false;
    tiles : any[];
    receivedData:Array<any> = [];
    varTitle : string;
    description : string;
    category : string;
    expirtyDate : any;
    assignee :string;
    pending=false;
    taskList;
    users:any[];

    //for date time picker
    todaydate = new Date();
    day=this.todaydate.getDate();
    month=this.todaydate.getMonth()+1;
    year=this.todaydate.getFullYear();

    myDatePickerOptions = {
        todayBtnTxt: 'Today',
        dateFormat: 'dd/mm/yyyy',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        inline: false,
        disableUntil: {year: this.year, month: this.month, day: this.day},
        selectionTxtFontSize: '14px'
    };

    constructor(private _TasksService : TasksService){
        this.categoryList=_TasksService.getTasks();
        this.tiles = _TasksService.getAssignees();
        this.users=_TasksService.getAllUsers();
        //this.taskList = this.getTaskList();
        // this._TasksService.find();
        // var $input = $('search');
        // vent($input, "keyup");
        // keyups.subscribe(data =>  console.log(data));
    }
    ngAfterViewInit(){
    var keyups = Observable.fromEvent($("#search"), "keyup")
    .map(e => e.target.value)
    .filter(text => text.length >= 3)
    .debounceTime(400)
    .distinctUntilChanged()
    .flatMap(searchTerm => {
        this.pending =true;
        this.users = [];
        var promise = this._TasksService.searchUsers(searchTerm);
        return Observable.fromPromise(promise);
    });
    keyups.subscribe(data => {console.log(data);
       this.users = [];
       data.forEach(element => {
           this.users.push(element);
       });
       this.pending =false;
       console.log(this.users);
    });

}
    onListClick(item){
        this.selectedItem = item;
        this.category= item;
    }
    addedNew(){
        this.clickedAddNew=!this.clickedAddNew;
    }
     onDateChanged(event:any) {
        console.log('onDateChanged(): ', event.date, ' - formatted: ', event.formatted, ' - epoc timestamp: ',
         event.epoc);
         this.expirtyDate=event.formatted;
    }

    transferDataSuccess($event) {
        this.receivedData.pop();
        this.receivedData.push($event.dragData.name);
        this.assignee=$event.dragData.name;
    }
    saveTask(){
        debugger;
        this._TasksService.saveTasks(this.varTitle,this.description,this.category,this.expirtyDate,this.assignee);
    }

    getTaskList(){
        this._TasksService.GetTasks();
        debugger;
    }

}