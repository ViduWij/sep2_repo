namespace Tu_Tu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BadgebIDdataType : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Badges");
            AlterColumn("dbo.Badges", "bId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Badges", "bId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Badges");
            AlterColumn("dbo.Badges", "bId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Badges", "bId");
        }
    }
}
