namespace Tu_Tu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dataTypeofDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Activities", "date", c => c.String());
            AlterColumn("dbo.Activities", "expiryDate", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Activities", "expiryDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Activities", "date", c => c.DateTime(nullable: false));
        }
    }
}
