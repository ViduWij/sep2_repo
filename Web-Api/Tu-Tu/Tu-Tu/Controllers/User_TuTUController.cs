﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Tu_Tu.Models;
using Tu_Tu.ViewModels;

namespace Tu_Tu.Controllers
{
    [Authorize]
    [RoutePrefix("api/User")]
    public class User_TuTUController : ApiController
    {
        private DBContext_Tutu db = new DBContext_Tutu();

        [AllowAnonymous]
        [HttpGet]
        [Route("GetAllUsers")]
        // GET: api/User_TuTU
        public IQueryable<User_TuTU> GetUser_TuTu()
        {
            return db.User_TuTu;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetDetails")]
        // GET: api/User_TuTU/5
        [ResponseType(typeof(User_TuTU))]
        public IHttpActionResult GetUser_TuTU(int id)
        {
            User_TuTU user_TuTU = db.User_TuTu.Find(id);
            if (user_TuTU == null)
            {
                return NotFound();
            }

            return Ok(user_TuTU);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetInfo")]
        // GET: api/User/
        [ResponseType(typeof(User_TuTU))]
        public IHttpActionResult GetUserDetails()
        {
            try
            {
                var userDetails = from u in db.User_TuTu.AsEnumerable()
                                  join a in db.Activities.AsEnumerable()
                                  on u.uid equals a.assignee
                                  where u.uid == 2 && u.uid == a.assignee
                                  select new
                                  {
                                      u.uid,
                                     fname= u.name.Split(' ')[0].ToString(),
                                     lname= u.name.Split(' ')[1].ToString(),
                                      u.password,
                                      u.companyName,
                                      u.email,
                                      adminName = (from e in db.User_TuTu
                                                  where e.uid == u.adminId
                                                  select e.name).FirstOrDefault(),
                                      a.title,
                                      a.starCount,
                                      a.date,
                                      assigner  = (from e in db.User_TuTu
                                                  where e.uid == a.assigner
                                                  select e.name).FirstOrDefault()                                   
            };

                
                string base64String = "";
                int uid = 2; // fix this..session.....!!!!!
                string filename = "User_" + uid.ToString();
                var imagePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Images/ProfilePics"), filename + ".jpg");

                if (!System.IO.File.Exists(imagePath))
                {
                    Debug.WriteLine("nopic"); //fix on client side
                }
                else
                {
                    using (Image image = Image.FromFile(imagePath))
                    {
                        using (MemoryStream m = new MemoryStream())
                        {
                            image.Save(m, image.RawFormat);
                            byte[] imageBytes = m.ToArray();
                            base64String = Convert.ToBase64String(imageBytes);
                        }
                    }
                }



                if (userDetails == null)
                {
                    return NotFound();
                }
             
                return Json(new { userDetails , avatar = base64String});
            //    return Json(userDetails);

            }
            catch (Exception e)
            {
                return NotFound();
            }
            // id = 1;
        }
          
       
		// PUT: api/User_TuTU/5
        [AllowAnonymous]
        [Route("UpdateInfo")]
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser_TuTU(int id, User_TutuBasicDetails user_TuTU)
        {
           // if (!ModelState.IsValid)
            //{
              //  return BadRequest(ModelState);
            //}

            if (id != user_TuTU.uid)
            {
                return BadRequest();
            }

          //  db.Entry(user_TuTU).State = EntityState.Modified;
            
            try
            {

                var User_TutuBasicDetails = db.User_TuTu.Where(a => a.uid.Equals(id)).FirstOrDefault();
                User_TutuBasicDetails.name = user_TuTU.name;
                User_TutuBasicDetails.email = user_TuTU.email;
                User_TutuBasicDetails.companyName = user_TuTU.companyName;
                User_TutuBasicDetails.adminId = user_TuTU.adminId;

                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!User_TuTUExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


		 // PUT: api/User_TuTU/5
        [AllowAnonymous]
        [Route("EditPrivacyInfo")]
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser_TuTUPrivacy(int id, User_TutuPrivacyVModel user_TuTU)
        {
            // if (!ModelState.IsValid)
            //{
            //  return BadRequest(ModelState);
            //}

            if (id != user_TuTU.uid)
            {
                return BadRequest();
            }

            //  db.Entry(user_TuTU).State = EntityState.Modified;

            try
            {
                var User_TutuDetails = db.User_TuTu.Where(a => a.uid.Equals(id)).FirstOrDefault();
                User_TutuDetails.password = user_TuTU.password;
            
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!User_TuTUExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
		
		
		 [AllowAnonymous]
        [Route("UploadPic")]
        [HttpPost]
        public IHttpActionResult UploadPic(User_TutuAvatarVModel userView)
         {
            //string userName = (from u in db.User_TuTu
            //                  where u.uid == userView.uid
            //                  select u.name).FirstOrDefault().ToString();

          //  string firstName = userName.Split(' ')[0];
            string fileName = "User_" + userView.uid.ToString();

            string imageBaseString = ((userView.avatar).ToString()).Substring(23);

            byte[] imageBytes = Convert.FromBase64String(imageBaseString);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
               Image image = Image.FromStream(ms, true);
               image.Save(System.Web.HttpContext.Current.Server.MapPath("~/Images/ProfilePics/" + fileName + ".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);

            }

            //  var targetPath = System.Web.HttpContext.Current.Server.MapPath("~/Images/ProfilePics");
            var targetPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Images/ProfilePics"), fileName + ".jpg");

            var userDetails = db.User_TuTu.Where(a => a.uid.Equals(userView.uid)).FirstOrDefault();

            //Updates the status of the trip.
            userDetails.avatar = targetPath;
             db.SaveChanges();


            return Json("Success!");
        }
		
		
        [AllowAnonymous]
        [HttpGet]
        [Route("GetBadgesInfo")]
        // GET: api/User/
        [ResponseType(typeof(User_TuTU))]
        public IHttpActionResult GetUserBadgeDetails()
        {
            try
            {
                int uId = 2; // get from the session

                var usersList = from u in db.User_TuTu
                                where u.uid != uId
                                select new
                                {
                                    u.uid,
                                    u.name
                                };


                var bestQuality = from b in db.userBadgeCount.AsEnumerable()
                                  group b by b.Badge into ub
                                  join u in db.Badges.AsEnumerable()
                                  on ub.FirstOrDefault().Badge equals u.bId
                                  where ub.FirstOrDefault().User == uId && u.name == "Best Quality"
                                  select new
                                  {
                                      badgeID = ub.FirstOrDefault().Badge,
                                      title = u.name,
                                      count = ub.Sum(c => c.count)
                                  };

                var goodQuality = from b in db.userBadgeCount.AsEnumerable()
                                  group b by b.Badge into ub
                                  join u in db.Badges.AsEnumerable()
                                  on ub.FirstOrDefault().Badge equals u.bId
                                  where ub.FirstOrDefault().User == uId && u.name == "Good Quality"
                                  select new
                                  {
                                      badgeID = ub.FirstOrDefault().Badge,
                                      title = u.name,
                                      count = (ub.Sum(c => c.count) == 0) ? 0 : ub.Sum(c => c.count)
                                  };

                var excellent = from b in db.userBadgeCount.AsEnumerable()
                                group b by b.Badge into ub
                                join u in db.Badges.AsEnumerable()
                                on ub.FirstOrDefault().Badge equals u.bId
                                where ub.FirstOrDefault().User == uId && u.name == "Excellent"
                                select new
                                {
                                    badgeID = ub.FirstOrDefault().Badge,
                                    title = u.name,
                                    count = ub.Sum(c => c.count)
                                };

                var creativity = from b in db.userBadgeCount.AsEnumerable()
                                  group b by b.Badge into ub
                                  join u in db.Badges.AsEnumerable()
                                  on ub.FirstOrDefault().Badge equals u.bId
                                  where ub.FirstOrDefault().User == uId && u.name == "Creativity"
                                 select new
                                  {
                                      badgeID = ub.FirstOrDefault().Badge,
                                     title = u.name,
                                     count = ub.Sum(c => c.count)
                                  };

                var goodJob = from b in db.userBadgeCount.AsEnumerable()
                                  group b by b.Badge into ub
                                  join u in db.Badges.AsEnumerable()
                                  on ub.FirstOrDefault().Badge equals u.bId
                                  where ub.FirstOrDefault().User == uId && u.name == "Good Job"
                                  select new
                                  {
                                      badgeID = ub.FirstOrDefault().Badge,
                                      title = u.name,
                                      count = ub.Sum(c => c.count)
                                  };


                return Json(new { uId, usersList, bestQuality, goodQuality, excellent, creativity, goodJob });
                //    return Json(userDetails);

            }
            catch (Exception e)
            {
                return NotFound();
            }
        }


		
		
        // POST: api/User_TuTU
        [ResponseType(typeof(User_TuTU))]
        public IHttpActionResult PostUser_TuTU(User_TuTU user_TuTU)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.User_TuTu.Add(user_TuTU);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = user_TuTU.uid }, user_TuTU);
        }

        // DELETE: api/User_TuTU/5
        [ResponseType(typeof(User_TuTU))]
        public IHttpActionResult DeleteUser_TuTU(int id)
        {
            User_TuTU user_TuTU = db.User_TuTu.Find(id);
            if (user_TuTU == null)
            {
                return NotFound();
            }

            db.User_TuTu.Remove(user_TuTU);
            db.SaveChanges();

            return Ok(user_TuTU);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool User_TuTUExists(int id)
        {
            return db.User_TuTu.Count(e => e.uid == id) > 0;
        }
    }
}